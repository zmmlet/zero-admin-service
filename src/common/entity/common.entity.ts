import {
  Column,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  CreateDateColumn,
  VersionColumn,
} from 'typeorm';

export abstract class Common {
  // 主键id
  // @PrimaryGeneratedColumn()
  // id: number;
  @PrimaryGeneratedColumn('uuid')
  id: string;

  // 创建时间
  @CreateDateColumn()
  create_time: Date;

  // 更新时间
  @UpdateDateColumn()
  update_time: Date;

  // 删除标记
  @Column({
    default: false,
    select: false,
    comment: '删除',
  })
  del_flag: boolean;

  // 更新次数
  @VersionColumn({
    select: false,
    comment: '更新次数',
  })
  version: number;

  @Column({
    comment: '备注',
    nullable: true,
    type: 'varchar',
  })
  remarks: string;
}
