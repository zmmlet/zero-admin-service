import { ApiProperty } from '@nestjs/swagger';
import { Matches } from 'class-validator';
import { regPositive } from 'src/utils/regex.util';

export class PaginationDTO {
  @ApiProperty({ description: '当前页' })
  @Matches(regPositive, { message: 'page 不可小于 0' })
  readonly page?: number;

  @ApiProperty({ description: '一页显示多少条' })
  @Matches(regPositive, { message: 'pageSize 不可小于 0' })
  readonly pageSize?: number;
}
