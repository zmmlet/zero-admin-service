import { Module } from '@nestjs/common';
import { ConfigService, ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import envConfig from 'config/env';
import { RoleModule } from './modules/role/role.module';
import { RolesEntity } from './modules/role/entity/role.entity';
import { UploadModule } from './modules/upload/upload.module';
import { UserEntity } from './modules/user/entity/user.entity';
import { UserModule } from './modules/user/user.module';
import { AuthModule } from './modules/auth/auth.module';
import { PermissionModule } from './modules/permission/permission.module';
import { PermissionEntity } from './modules/permission/entity/permission.entity';
import { RolePermissionModule } from './modules/rolePermission/role-permission.module';
import { RolePermissionEntity } from './modules/rolePermission/entity/role-permission.entity';
import { UploadEntity } from './modules/upload/entity/upload.entity';
import { UserRoleModule } from './modules/userRole/user-role.module';
import { UserRoleEntity } from './modules/userRole/entity/user-role.entity';
import { ScheduleModule } from '@nestjs/schedule';
import { TasksModule } from './modules/tasks/tasks.module';
import { EmailModule } from './modules/email/email.module';
import { MakeapieModule } from './modules/makeapie/makeapie.module';
import { MakeapieEntity } from './modules/makeapie/entity/makeapie.entity';

/**
 * @desc T应用程序的根模块（root module）
 * */
// @Module() 装饰器接收四个属性：providers、controllers、imports、exports
@Module({
  // imports：导入模块的列表，如果需要使用其他模块的服务，需要通过这里导入
  imports: [
    // 设置数据库变量为全局
    ConfigModule.forRoot({
      isGlobal: true, // 设置为全局
      envFilePath: [envConfig.path],
    }),
    // 使用 TypeORM 配置数据库
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get('DB_HOST', 'localhost'), // 主机，默认为localhost,
        port: configService.get<number>('DB_PORT', 3306), // 端口号,
        username: configService.get('DB_USER', 'root'), // 用户名
        password: configService.get('DB_PASSWORD', 'root'), // 密码
        database: configService.get('DB_DATABASE'), //数据库名
        entities: [
          RolesEntity,
          UserEntity,
          PermissionEntity,
          UserRoleEntity,
          RolePermissionEntity,
          UploadEntity,
          MakeapieEntity,
        ], // 数据表实体
        timezone: '+08:00', //服务器上配置的时区
        synchronize: true, //根据实体自动创建数据库表， 生产环境建议关闭
      }),
    }),

    // 系统模块
    UserModule,
    AuthModule,
    RoleModule,
    UserRoleModule,
    PermissionModule,
    RolePermissionModule,
    // 文件模块
    UploadModule,
    // 定时任务
    // ScheduleModule.forRoot(),
    // TasksModule,
    // 邮箱
    EmailModule,
    MakeapieModule,
  ],
  // controllers：处理http请求，包括路由控制，向客户端返回响应，将具体业务逻辑委托给providers处理
  controllers: [],
  // providers：Nest.js注入器实例化的提供者（服务提供者），处理具体的业务逻辑，各个模块之间可以共享（注入器的概念后面依赖注入部分会讲解）
  providers: [],
})
// exports：导出服务的列表，供其他模块导入使用。如果希望当前模块下的服务可以被其他模块共享，需要在这里配置导出
export class AppModule {}
