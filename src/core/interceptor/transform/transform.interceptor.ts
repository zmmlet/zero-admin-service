import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { map, Observable } from 'rxjs';

@Injectable()
export class TransformInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    // const methodType = context['args'][0].method;
    // const url = context['args'][0].url;
    return next.handle().pipe(
      map((data) => {
        const result: any = {
          data: data,
          code: 200,
          success: true,
        };
        // switch (methodType) {
        //   case 'GET':
        //     result.data = data;
        //     result.message = '请求成功！';
        //     break;
        //   case 'POST':
        //     result.message = '新增成功！';
        //     break;
        //   case 'PUT':
        //     result.message = '编辑成功！';
        //     break;
        //   case 'DELETE':
        //     result.message = '删除成功！';
        //     break;
        //   default:
        //     result.message = '操作成功！';
        //     break;
        // }
        // if (url == '/zero-api/auth/login') {
        //   result.message = '登录成功！';
        //   result.data = { result: data };
        // }
        // if (url == '/zero-api/user/register') {
        //   result.message = '注册成功！';
        //   result.data = { result: data };
        // }
        return result;
      }),
    );
  }
}
