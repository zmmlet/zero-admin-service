import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './core/filter/http-exception/http-exception.filter';
import { TransformInterceptor } from './core/interceptor/transform/transform.interceptor';

import Chalk = require('chalk');
import helmet from 'helmet';
const Log = console.log;

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  // 注册全局过滤器-拦截错误请求
  app.useGlobalFilters(new HttpExceptionFilter());
  // 拦截成功返回数据
  app.useGlobalInterceptors(new TransformInterceptor());
  // 设置全局路由前缀
  app.setGlobalPrefix('zero-api');
  // 全局注册一下管道ValidationPipe进行数据验证
  app.useGlobalPipes(new ValidationPipe());
  // 配置静态资源访问目录 上传文件后，根据url访问静态资源
  app.useStaticAssets('uploads', { prefix: '/uploads' });

  // web 安全，防常见漏洞
  app.use(helmet());

  // 配置swagger文档
  const swaggerConfig = new DocumentBuilder()
    .setTitle('管理后台')
    .setDescription('管理后台接口文档')
    .setVersion('v1.0')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('docs', app, document);
  // 设置端口
  await app.listen(9030);
  Log(
    Chalk.green(`Zero-Admin-Service 服务启动成功 `),
    Chalk.blue(`http://localhost:9030/zero-api`),
    '\n',
    Chalk.green('Zero-Admin-Swagger-Docs 地址   '),
    Chalk.blue(`http://localhost:9030/docs`),
  );
}
bootstrap();
