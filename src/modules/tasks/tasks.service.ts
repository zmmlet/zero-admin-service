import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cron, Interval, Timeout } from '@nestjs/schedule';
import { EmailService } from '../email/email.service';

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  constructor(
    private readonly emailService: EmailService,
    private readonly configService: ConfigService,
  ) {}
  /* 
  * * * * * * 分别对应的意思：
  第1个星：秒
  第2个星：分钟
  第3个星：小时
  第4个星：一个月中的第几天
  第5个星：月
  第6个星：一个星期中的第几天

  如：
  45 * * * * *：该方法将在45秒标记处每分钟运行一次

  */
  @Cron('45 * * * * *')
  handleCron() {
    this.logger.debug('该方法将在45秒标记处每分钟运行一次');
    this.emailService.sendEmailCode({
      email: this.configService.get<string>('TEST_EMAIL'),
      subject: this.configService.get<string>('TEST_SUBJECT'),
    });
  }

  @Interval(10000)
  handleInterval() {
    this.logger.debug('2');
  }

  @Timeout(5000)
  handleTimeout() {
    this.logger.debug('3');
  }

  @Interval(10000)
  sendEmail() {
    this.logger.debug('4');
  }
}
