import { Module } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { ScheduleModule } from '@nestjs/schedule';
import { EmailService } from '../email/email.service';

@Module({
  providers: [TasksService, EmailService],
  imports: [ScheduleModule.forRoot()],
})
export class TasksModule {}
