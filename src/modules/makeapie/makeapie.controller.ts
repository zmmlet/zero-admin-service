import { Controller, Get, Query } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ListDTO } from './dto/markeapie.dto';
import { MakeapieService } from './makeapie.service';

@ApiTags('chart')
@Controller('makeapie')
export class MakeapieController {
  constructor(private readonly makeapieService: MakeapieService) {}

  @ApiOperation({ summary: '获取Echarts社区资源' })
  @Get('findAllEcharts')
  findAllEcharts(@Query() list: ListDTO) {
    return this.makeapieService.findAllEcharts(list);
  }
}
