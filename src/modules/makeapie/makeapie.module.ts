import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MakeapieEntity } from './entity/makeapie.entity';
import { MakeapieController } from './makeapie.controller';
import { MakeapieService } from './makeapie.service';

@Module({
  imports: [TypeOrmModule.forFeature([MakeapieEntity])],
  providers: [MakeapieService],
  controllers: [MakeapieController],
})
export class MakeapieModule {}
