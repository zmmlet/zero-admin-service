import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('chart_community')
export class MakeapieEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    comment: 'cid',
    nullable: false,
    type: 'varchar',
  })
  cid: string;

  @Column({
    comment: '作者UID',
    nullable: true,
    type: 'varchar',
  })
  authorUid: string;

  @Column({
    comment: '作者用户名称',
    nullable: true,
    type: 'varchar',
  })
  authorUserName: string;

  @Column({
    comment: '标题',
    nullable: true,
    type: 'varchar',
  })
  title: string;

  @Column({
    comment: '描述',
    nullable: true,
    type: 'varchar',
  })
  description: string;

  @Column({
    comment: '最新版本\n\n',
    nullable: true,
    type: 'varchar',
  })
  latestVersion: string;

  @Column({
    comment: '总是最新的\n\n',
    nullable: true,
    type: 'varchar',
  })
  alwaysLatest: string;

  // 创建时间
  @CreateDateColumn()
  createTime: Date;

  // 更新时间
  @UpdateDateColumn()
  lastUpdateTime: Date;

  @Column({
    comment: '',
    nullable: true,
    type: 'float',
  })
  auth: number;

  @Column({
    comment: 'UID',
    nullable: true,
    type: 'varchar',
  })
  uid: string;

  @Column({
    comment: '出版版本',
    nullable: true,
    type: 'varchar',
  })
  publishedVersion: string;

  @Column({
    comment: '分支',
    nullable: true,
    type: 'varchar',
  })
  forkFrom: string;

  @Column({
    comment: 'isSpam',
    nullable: true,
    type: 'int',
  })
  isSpam: number;

  @Column({
    comment: '版本',
    nullable: true,
    type: 'varchar',
  })
  version: string;

  @Column({
    comment: '父版本',
    nullable: true,
    type: 'varchar',
  })
  parentVersion: string;

  @Column({
    comment: 'echarts版本',
    nullable: true,
    type: 'varchar',
  })
  echartsVersion: string;

  // 版本创建时间
  @CreateDateColumn()
  versionCreateTime: Date;

  @Column({
    comment: '代码',
    nullable: true,
    type: 'longtext',
  })
  code: string;

  @Column({
    comment: 'html',
    nullable: true,
    type: 'longtext',
  })
  html: string;

  @Column({
    comment: '外部脚本\n\n',
    nullable: true,
    type: 'longtext',
  })
  externalScripts: string;

  @Column({
    comment: '更新UID',
    nullable: true,
    type: 'varchar',
  })
  updaterUID: string;

  @Column({
    comment: '主题',
    nullable: true,
    type: 'varchar',
  })
  theme: string;

  @Column({
    comment: '布局',
    nullable: true,
    type: 'varchar',
  })
  layout: string;

  @Column({
    comment: '查看次数',
    nullable: true,
    type: 'int',
  })
  viewCount: string;

  @Column({
    comment: '用户名称',
    nullable: true,
    type: 'varchar',
  })
  userName: string;

  @Column({
    comment: '评论次数',
    nullable: true,
    type: 'varchar',
  })
  commentCount: string;

  @Column({
    comment: '点赞次数',
    nullable: true,
    type: 'int',
  })
  starCount: string;

  @Column({
    comment: '是否被关注',
    nullable: true,
    type: 'varchar',
  })
  isStared: string;

  @Column({
    comment: '图表图片',
    nullable: true,
    type: 'varchar',
  })
  thumbnailURL: string;

  @Column({
    comment: '是否自定义缩略图',
    nullable: true,
    type: 'varchar',
  })
  isCustomThumbnail: string;

  @Column({
    comment: '内置标签\n\n',
    nullable: true,
    type: 'varchar',
  })
  builtinTags: string;

  @Column({
    comment: '自定义标签',
    nullable: true,
    type: 'varchar',
  })
  customTags: string;

  @Column({
    comment: '更新用户名称',
    nullable: true,
    type: 'varchar',
  })
  updaterUserName: string;

  @Column({
    comment: '头像ID(https://himg.bdimg.com/sys/portrait/item/id.jpg)',
    nullable: true,
    type: 'varchar',
  })
  avatar: string;
}
