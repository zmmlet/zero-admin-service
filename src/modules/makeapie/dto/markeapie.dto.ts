import { PaginationDTO } from 'src/common/dto/common.dto';

export class ListDTO extends PaginationDTO {}
