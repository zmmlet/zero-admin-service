import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { DictService } from './dict.service';

@ApiTags('字典管理')
@Controller('dict')
export class DictController {
  constructor(private readonly rolesService: DictService) {}
}
