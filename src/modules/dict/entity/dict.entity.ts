import { ApiPropertyOptional } from '@nestjs/swagger/dist';
import { Common } from 'src/common/entity/common.entity';

export class DictEntity extends Common {
  @ApiPropertyOptional({ description: '排序' })
  readonly sort: number;
}
