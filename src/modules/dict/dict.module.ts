import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DictController } from './dict.controller';
import { DictService } from './dict.service';
import { DictEntity } from './entity/dict.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DictEntity])],
  providers: [DictService],
  controllers: [DictController],
})
export class DictModule {}
