import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { PaginationDTO } from 'src/common/dto/common.dto';

export class CreatePermissionDto {
  @ApiPropertyOptional({ description: '父级id' })
  readonly parent_id: string;

  @ApiProperty({ description: '菜单标题' })
  @IsNotEmpty({ message: '菜单标题必填' })
  readonly name: string;

  @ApiProperty({ description: '菜单路径' })
  @IsNotEmpty({ message: '菜单路径必填' })
  readonly url: string;

  @ApiProperty({ description: '前端组件' })
  @IsNotEmpty({ message: '前端组件必填' })
  readonly component: string;

  @ApiPropertyOptional({ description: '组件名称' })
  readonly component_name: string;

  @ApiPropertyOptional({
    description: '菜单类型默认传递0(0:一级菜单; 1:子菜单:2:按钮权限)',
  })
  readonly menu_type: number;

  @ApiPropertyOptional({ description: '菜单排序' })
  readonly sort: number;

  @ApiPropertyOptional({ description: '菜单图标' })
  readonly icon: string;

  @ApiPropertyOptional({ description: '是否路由菜单: 0:不是  1:是（默认值1）' })
  readonly is_route: number;

  @ApiPropertyOptional({ description: '是否缓存该页面: 1:是 0:不是' })
  readonly keep: number;

  @ApiPropertyOptional({ description: '是否隐藏路由: 0否,1是' })
  readonly hidden: number;

  @ApiPropertyOptional({ description: '按钮权限状态(0无效1有效)' })
  readonly status: string;

  @ApiPropertyOptional({ description: '描述' })
  readonly description: string;
}

export class EditPermissionDto extends CreatePermissionDto {}

export class IdDto {
  @ApiProperty({ description: '菜单id' })
  @IsNotEmpty({ message: '菜单id必填' })
  readonly id: string;
}

export class ListDTO extends PaginationDTO {
  // 筛选条件非必穿
  @ApiPropertyOptional({ required: false, description: '菜单名称' })
  @IsOptional()
  readonly name?: string;
}
