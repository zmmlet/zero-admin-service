import { Common } from 'src/common/entity/common.entity';
import { Column, Entity } from 'typeorm';
/* 
https://blog.csdn.net/yehuozhili/article/details/108373529
type配置字段类型,在mysql中字符类型可能是char、varchar、text,数字类型可能是int、tinyint,小数类型可能是float、double、decimal(10,2)等
name真正映射到mysql数据库中字段名字,如果不指定会默认以对象的字段为名字(建议都指定)
length长度,比如在mysql中字段为varchar的时候指定字段长度
nullable在mysql中字段是否可以为NULL值,默认为false
select改字段是否可以被查询出来(针对使用typeORM的查寻操作,不针对你使用原生SQL语句操作),默认为true表示可以被查询出来
default默认值,比如插入数据的时候,没传递该字段的值,就默认一个值
unique是否唯一约束
comment备注该字段是做什么的(建议都写上,方便阅读)
enum枚举类型
array该列是否以数组
*/
// 菜单权限表
@Entity('sys_permission')
export class PermissionEntity extends Common {
  @Column({
    length: 100,
    comment: '父级id',
    nullable: true,
    // default: null,
    type: 'varchar',
  })
  parent_id: string;

  @Column({
    length: 100,
    comment: '菜单标题',
    nullable: false,
    type: 'varchar',
  })
  name: string;

  @Column({
    comment: '路径',
    nullable: false,
    type: 'varchar',
  })
  url: string;

  @Column({
    comment: '组件',
    nullable: true,
    type: 'varchar',
  })
  component: string;

  @Column({
    comment: '组件名称',
    nullable: true,
    type: 'text',
  })
  component_name: string;

  @Column({
    comment: '菜单类型(0:一级菜单; 1:子菜单:2:按钮权限)',
    nullable: true,
    type: 'int',
  })
  menu_type: number;

  @Column({
    comment: '菜单权限编码',
    nullable: true,
    type: 'varchar',
  })
  perms: string;

  @Column({
    comment: '权限策略1显示2禁用',
    nullable: true,
    type: 'varchar',
  })
  perms_type: string;

  @Column({
    comment: '菜单排序',
    nullable: true,
    type: 'int',
  })
  sort: number;

  @Column({
    length: 100,
    comment: '菜单图标',
    nullable: true,
    type: 'varchar',
  })
  icon: string;

  @Column({
    comment: '是否路由菜单: 0:不是  1:是（默认值1）',
    nullable: true,
    type: 'tinyint',
  })
  is_route: number;

  @Column({
    comment: '是否叶子节点: 1:是 0:不是',
    nullable: true,
    type: 'tinyint',
  })
  is_leaf: number;

  @Column({
    comment: '是否缓存该页面: 1:是 0:不是',
    nullable: true,
    type: 'tinyint',
  })
  keep: number;

  @Column({
    comment: '是否隐藏路由: 0否,1是',
    nullable: true,
    type: 'int',
  })
  hidden: number;

  @Column({
    length: 2,
    comment: '按钮权限状态(0无效1有效)',
    nullable: true,
    type: 'varchar',
  })
  status: string;

  @Column({
    comment: '描述',
    nullable: true,
    type: 'text',
  })
  description: string;
}
