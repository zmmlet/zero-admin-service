import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import {
  CreatePermissionDto,
  EditPermissionDto,
  IdDto,
  ListDTO,
} from './dto/permission.dto';
import { PermissionService } from './permission.service';

@ApiTags('菜单管理')
@Controller('permission')
export class PermissionController {
  constructor(private readonly permissionService: PermissionService) {}

  /**
   * 新建菜单
   * @param post
   */
  @ApiOperation({ summary: '新建菜单' })
  @Post('add')
  async create(@Body() post: CreatePermissionDto) {
    return await this.permissionService.create(post);
  }

  /**
   * 获取所有角色
   */
  @ApiOperation({ summary: '获取菜单列表' })
  @Get('pageList')
  getPageList(@Query() list: ListDTO) {
    return this.permissionService.findAll(list);
  }

  /**
   * 获取菜单权限树
   * @param post
   */
  @ApiOperation({ summary: '获取菜单权限树' })
  @Get('tree')
  async getPermissionTree() {
    return await this.permissionService.getPermissionTree();
  }

  /**
   * 根据登录用户id获取对应权限菜单
   * @param post
   */
  @ApiOperation({ summary: '根据登录用户id获取对应权限菜单' })
  @Get('getUserPermission')
  async getUserPermission(@Query() query: IdDto) {
    return await this.permissionService.getUserPermission(query);
  }

  /**
   * 获取菜单角色
   * @param id
   */
  @ApiOperation({ summary: '根据id获取菜单信息' })
  @Get('queryById')
  async findById(@Query() query: IdDto) {
    return await this.permissionService.findById(query);
  }

  /**
   * 编辑
   * @param id
   * @param editParams
   */
  @ApiOperation({ summary: '编辑菜单' })
  @Put('edit:id')
  update(@Param('id') id: string, @Body() body: EditPermissionDto) {
    return this.permissionService.updateById(id, body);
  }

  /**
   * @desc 删除
   * @param query
   * @returns
   */
  @ApiOperation({ summary: '根据id删除菜单' })
  @Delete('delete')
  async remove(@Query() query: IdDto) {
    return await this.permissionService.remove(query);
  }
}
