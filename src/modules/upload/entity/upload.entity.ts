'文件所属功能';
import { Common } from 'src/common/entity/common.entity';
import { Column, Entity } from 'typeorm';

@Entity('file_upload')
export class UploadEntity extends Common {
  @Column({
    comment: '所属ID',
    nullable: false,
    type: 'varchar',
  })
  attribution_id: string;

  @Column({
    comment: '文件所属功能',
    nullable: true,
    type: 'varchar',
  })
  file_type: string;

  @Column({
    comment: '文件类型名称',
    nullable: true,
    type: 'varchar',
  })
  type_name: string;

  @Column({
    comment: '文件后缀',
    nullable: true,
    type: 'varchar',
  })
  file_suffix: string;

  @Column({
    comment: '文件名称',
    nullable: true,
    type: 'varchar',
  })
  file_name: string;

  @Column({ type: 'varchar', comment: '文件 url' })
  public url: string;

  @Column({
    comment: '文件路径',
    nullable: true,
    type: 'varchar',
  })
  file_src: string;

  @Column({
    comment: '文件大小',
    nullable: true,
    type: 'varchar',
  })
  file_size: string;

  @Column({
    comment: '上传用户',
    type: 'varchar',
  })
  create_by: string;
}
