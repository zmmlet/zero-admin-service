import { ApiProperty } from '@nestjs/swagger';
import { IsDateString, IsOptional } from 'class-validator';
import { PaginationDTO } from 'src/common/dto/common.dto';

export class ListDTO extends PaginationDTO {
  // 筛选条件非必穿
  @ApiProperty({ description: '搜索条件，起始时间', required: false })
  @IsDateString()
  @IsOptional()
  startDay?: string;

  @ApiProperty({ description: '搜索条件，结束时间', required: false })
  @IsDateString()
  @IsOptional()
  endDay?: string;
}
