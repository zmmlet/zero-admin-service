import { Module } from '@nestjs/common';
import { UploadService } from './upload.service';
import { UploadController } from './upload.controller';
import { UploadEntity } from './entity/upload.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname, join } from 'path';
import { ConfigService } from '@nestjs/config';

const multerModule = MulterModule.registerAsync({
  inject: [ConfigService],
  useFactory: async (configService: ConfigService) => {
    return {
      dest: join(__dirname, configService.get('UPLOAD_FOLDER')),
      preservePath: true,
      // storage: diskStorage({
      //   // 文件存储位置
      //   destination: 'uploads',
      //   //文件名定制
      //   filename: (req, file, callback) => {
      //     const path =
      //       Date.now() +
      //       '-' +
      //       Math.round(Math.random() * 1e10) +
      //       extname(file.originalname);
      //     callback(null, path);
      //   },
      // }),
    };
  },
});

@Module({
  imports: [
    TypeOrmModule.forFeature([UploadEntity]),
    multerModule,
    // MulterModule.registerAsync({
    //   useFactory() {
    //     return {
    //       storage: diskStorage({
    //         // 文件存储位置
    //         destination: 'uploads',
    //         //文件名定制
    //         filename: (req, file, callback) => {
    //           const path =
    //             Date.now() +
    //             '-' +
    //             Math.round(Math.random() * 1e10) +
    //             extname(file.originalname);
    //           callback(null, path);
    //         },
    //       }),
    //     };
    //   },
    // }),
  ],
  controllers: [UploadController],
  providers: [UploadService],
})
export class UploadModule {}
