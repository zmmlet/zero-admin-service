import {
  Controller,
  Post,
  UseInterceptors,
  UploadedFile,
  HttpCode,
  Body,
  Req,
} from '@nestjs/common';
import { UploadService } from './upload.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBody, ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiTags('文件上传')
@Controller('upload')
// @UseInterceptors(new TransformInterceptor()) //请求拦截器
export class UploadController {
  constructor(private readonly uploadService: UploadService) {}

  @Post('file')
  @ApiOperation({ summary: '文件上传（开发中...）' })
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('file'))
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          description: '文件',
          type: 'string',
          format: 'binary',
        },
        remarks: {
          description: '上传文件描述，可以是纯字符串，也可以是JSON字符串',
          type: 'string',
          format: 'text',
        },
      },
    },
  })
  @HttpCode(200)
  async uploadFile(
    @UploadedFile() file: Express.Multer.File,
    @Body() params: { remarks: string },
    @Req() req,
  ): Promise<any> {
    return await this.uploadService.create(
      [file],
      params.remarks || '',
      req.user,
    );
  }

  @Post('UP')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('file'))
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          description: '文件',
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @HttpCode(200)
  async uploadFqile(@UploadedFile() file: Express.Multer.File): Promise<any> {
    return this.uploadService.uploadFile(file);
  }
}
