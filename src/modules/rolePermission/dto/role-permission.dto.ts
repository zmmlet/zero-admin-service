import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class RolePermissionDto {
  @ApiProperty({ description: '角色ID' })
  @IsNotEmpty({ message: '角色ID必填' })
  role_id: string;

  @ApiProperty({ description: '菜单权限ID' })
  @IsNotEmpty({ message: '菜单权限ID必填' })
  permission_id: string;
}

export class IdDto {
  @ApiProperty({ description: '角色id' })
  @IsNotEmpty({ message: '角色id必填' })
  readonly id: string;
}
