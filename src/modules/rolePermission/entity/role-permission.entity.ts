import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

// 菜单权限表
@Entity('sys_role_permission')
export class RolePermissionEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    length: 100,
    comment: '角色id',
    type: 'varchar',
  })
  role_id: string;

  @Column({
    length: 100,
    comment: '权限id',
    type: 'varchar',
  })
  permission_id: string;
}
