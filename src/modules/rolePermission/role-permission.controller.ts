import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { IdDto, RolePermissionDto } from './dto/role-permission.dto';
import { RolePermissionService } from './role-permission.service';

@ApiTags('角色权限管理')
@Controller('permission')
export class RolePermissionController {
  constructor(private readonly rolePermissionService: RolePermissionService) {}

  /**
   * 关联角色和菜单权限数据
   * @param post
   */
  @ApiOperation({ summary: '关联角色和菜单权限数据' })
  @Post('saveRolePermission')
  async create(@Body() post: RolePermissionDto) {
    return await this.rolePermissionService.create(post);
  }

  /**
   * 获取指定角色
   * @param id
   */
  @ApiOperation({ summary: '根据角色id查询关联权限菜单id' })
  @Get('queryRoleList')
  async findById(@Query() query: IdDto) {
    return await this.rolePermissionService.queryRoleById(query);
  }
}
