import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  UseInterceptors,
  ClassSerializerInterceptor,
  UseGuards,
  Query,
  Put,
} from '@nestjs/common';
import { UserService } from './user.service';
import {
  CreateUserDto,
  ListUserDTO,
  queryByIdDto,
} from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { UserInfoDto } from './dto/user-info.dto';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('用户')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiOperation({ summary: '注册用户' })
  @ApiResponse({ status: 201, type: UserInfoDto })
  @UseInterceptors(ClassSerializerInterceptor)
  @Post('register')
  register(@Body() createUser: CreateUserDto) {
    return this.userService.register(createUser);
  }

  @ApiOperation({ summary: '获取用户列表' })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('pageList')
  getPageList(@Query() list: ListUserDTO) {
    return this.userService.findAll(list);
  }

  /**
   * @desc 获取指定用户
   * @param query
   * @returns
   */
  @ApiOperation({ summary: '根据id获取用户信息' })
  @Get('queryById')
  async findById(@Query() query: queryByIdDto) {
    return await this.userService.findById(query);
  }

  /**
   * @desc 编辑用户
   * @param id
   * @param body
   * @returns
   */
  @ApiOperation({ summary: '编辑用户' })
  @Put('edit:id')
  update(@Param('id') id: string, @Body() body: UpdateUserDto) {
    return this.userService.update(id, body);
  }

  /**
   * @desc 删除
   * @param query
   * @returns
   */
  @ApiOperation({ summary: '根据id删除用户' })
  @Delete('delete')
  async remove(@Query() query: queryByIdDto) {
    return await this.userService.remove(query);
  }
}
