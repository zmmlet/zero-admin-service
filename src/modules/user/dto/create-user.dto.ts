import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, Matches } from 'class-validator';
import { PaginationDTO } from 'src/common/dto/common.dto';
import { regPositive } from 'src/utils/regex.util';

export class CreateUserDto {
  @ApiProperty({ description: '用户名' })
  @IsNotEmpty({ message: '用户名必填' })
  username: string;

  @ApiPropertyOptional({ description: '昵称' })
  nickname: string;

  @IsNotEmpty({ message: '密码必填' })
  @ApiProperty({ description: '密码' })
  password: string;

  @ApiPropertyOptional({ description: '头像' })
  avatar: string;

  @IsNotEmpty({ message: '邮箱必填' })
  @ApiProperty({ description: '邮箱' })
  email: string;
}

export class queryByIdDto {
  @ApiProperty({ description: '用户id' })
  @IsNotEmpty({ message: '用户id必填' })
  readonly id: string;
}

export class ListUserDTO extends PaginationDTO {
  // 筛选条件非必穿
  @ApiPropertyOptional({ required: false, description: '用户名称' })
  @IsOptional()
  readonly username?: string;
  @ApiPropertyOptional({ required: false, description: '邮箱' })
  @IsOptional()
  readonly email?: string;
}
