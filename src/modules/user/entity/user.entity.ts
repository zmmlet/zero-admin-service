import { Common } from 'src/common/entity/common.entity';
import { BeforeInsert, Column, Entity } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import { Exclude } from 'class-transformer';

@Entity('sys_user')
export class UserEntity extends Common {
  @Column({ length: 100, comment: '用户名' })
  username: string;

  @Column({ length: 100, nullable: true, comment: '昵称' })
  nickname: string;

  @Exclude()
  @Column({ comment: '密码', type: 'text' })
  password: string;

  @Column({ comment: '头像', nullable: true, type: 'varchar' })
  avatar: string;

  @Column({ length: 50, comment: '邮箱', nullable: true, type: 'varchar' })
  email: string;

  @Column('simple-enum', { enum: ['root', 'author', 'visitor'] })
  role: string;

  // 密码加密
  @BeforeInsert()
  async encryptPwd() {
    if (!this.password) return;
    this.password = await bcrypt.hashSync(this.password, 10);
  }
}
