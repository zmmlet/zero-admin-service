import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import {
  CreateRoleDto,
  EditRoleDto,
  ListDTO,
  queryByIdDto,
} from './dto/create-role.dto';
import { RoleService } from './role.service';

/**
 * @desc 采用REST风格实现接口，设置请求路由，处理接口请求，调用相关服务完成业务逻辑
 */
@ApiTags('角色管理')
@Controller('role')
export class RoleController {
  constructor(private readonly rolesService: RoleService) {}

  /**
   * 创建角色
   * @param post
   */
  @ApiOperation({ summary: '创建角色' })
  @Post('add')
  async create(@Body() post: CreateRoleDto) {
    return await this.rolesService.create(post);
  }

  /**
   * 获取所有角色
   */
  @ApiOperation({ summary: '获取角色列表' })
  @Get('pageList')
  getPageList(@Query() list: ListDTO) {
    return this.rolesService.findAll(list);
  }

  /**
   * 获取指定角色
   * @param id
   */
  @ApiOperation({ summary: '根据id获取角色信息' })
  @Get('queryById')
  async findById(@Query() query: queryByIdDto) {
    return await this.rolesService.findById(query);
  }

  /**
   * 编辑
   * @param id
   * @param editParams
   */
  @ApiOperation({ summary: '编辑角色' })
  @Put('edit:id')
  update(@Param('id') id: string, @Body() body: EditRoleDto) {
    return this.rolesService.updateById(id, body);
  }

  /**
   * 删除
   * @param id
   */
  @ApiOperation({ summary: '根据id删除角色' })
  @Delete('delete')
  async remove(@Query() query: queryByIdDto) {
    return await this.rolesService.remove(query);
  }
}
