import { Common } from 'src/common/entity/common.entity';
import { Column, Entity } from 'typeorm';

@Entity('sys_roles')
export class RolesEntity extends Common {
  // 角色名称
  @Column({
    length: 200,
    comment: '角色名称',
    nullable: false,
    type: 'varchar',
  })
  role_name: string;

  // 角色编码
  @Column({
    length: 100,
    comment: '角色编码',
    nullable: false,
    type: 'varchar',
  })
  role_code: string;

  // 描述
  @Column({ comment: '描述', type: 'text', nullable: true })
  description: string;

  // 创建人
  @Column({ length: 100, comment: '创建人' })
  create_by: string;

  // 角色权限
  @Column({ length: 100, nullable: true, comment: '角色权限' })
  data_power: string;
}
