import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { PaginationDTO } from 'src/common/dto/common.dto';

export class CreateRoleDto {
  @ApiProperty({ description: '角色名称' })
  @IsNotEmpty({ message: '角色名称必填' })
  readonly role_name: string;

  @ApiProperty({ description: '角色编码' })
  @IsNotEmpty({ message: '角色编码必填' })
  readonly role_code: string;

  @ApiPropertyOptional({ description: '描述' })
  readonly description: string;

  @IsNotEmpty({ message: '创建人必填' })
  @ApiProperty({ description: '创建人' })
  readonly create_by: string;
}

export class EditRoleDto extends CreateRoleDto {}

export class queryByIdDto {
  @ApiProperty({ description: '角色id' })
  @IsNotEmpty({ message: '角色id必填' })
  readonly id: string;
}

export class ListDTO extends PaginationDTO {
  // 筛选条件非必穿
  @ApiPropertyOptional({ required: false, description: '角色名称' })
  @IsOptional()
  readonly role_name?: string;
}
