import { MailerModule } from '@nestjs-modules/mailer';
import { PugAdapter } from '@nestjs-modules/mailer/dist/adapters/pug.adapter';

import { Module } from '@nestjs/common';
import { EmailService } from './email.service';
import { EmailController } from './email.controller';
import { ConfigService } from '@nestjs/config';

const mailerModule = MailerModule.forRootAsync({
  inject: [ConfigService],
  useFactory: async (configService: ConfigService) => {
    return {
      transport: {
        host: configService.get<string>('SMTP_EMAIL'), //QQ邮箱的 smtp 服务器地址
        port: configService.get<number>('PORT_EMAIL'), //服务器端口 默认 465
        secureConnection: false, //是否使用对 https 协议的安全连接
        auth: {
          user: configService.get<string>('USER_EMAIL'), //你的邮箱地址
          pass: configService.get<string>('PASS_EMAIL'),
        },
      },
      defaults: {
        from: configService.get<string>('USER_EMAIL'), //发送人 你的邮箱地址
      },
      preview: true, //是否开启预览，开启了这个属性，在调试模式下会自动打开一个网页，预览邮件
      // 预览模板
      template: {
        dir: __dirname + '/template/email', // 模板文件夹路径
        adapter: new PugAdapter(),
        options: {
          strict: true, //严格模式
        },
      },
    };
  },
});

@Module({
  imports: [
    mailerModule,
    // MailerModule.forRoot({
    //   transport: {
    //     host: 'smtp.qq.com', //QQ邮箱的 smtp 服务器地址
    //     port: 465, //服务器端口 默认 465
    //     secureConnection: false, //是否使用对 https 协议的安全连接
    //     auth: {
    //       user: 'zmmlet@qq.com', //你的邮箱地址
    //       pass: 'sfiyixyalaoldecc',
    //     },
    //   },
    //   defaults: {
    //     from: 'zmmlet@qq.com', //发送人 你的邮箱地址
    //   },
    //   preview: true, //是否开启预览，开启了这个属性，在调试模式下会自动打开一个网页，预览邮件
    //   // 预览模板
    //   template: {
    //     dir: __dirname + '/template/email', // 模板文件夹路径
    //     adapter: new PugAdapter(),
    //     options: {
    //       strict: true, //严格模式
    //     },
    //   },
    // }),
  ],
  providers: [EmailService],
  controllers: [EmailController],
  exports: [EmailService],
})
export class EmailModule {}
