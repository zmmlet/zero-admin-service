import { Body, Controller, Post } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { EmailDto } from './dto/email.dto';
import { EmailService } from './email.service';

@ApiTags('邮件推送')
@Controller('email')
export class EmailController {
  constructor(private emailService: EmailService) {}

  @ApiOperation({ summary: '发送邮件' })
  @Post('/sendEmail')
  async sendEmailCode(@Body() data: EmailDto) {
    return this.emailService.sendEmailCode(data);
  }
}
