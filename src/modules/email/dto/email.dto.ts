import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class EmailDto {
  @ApiProperty({ description: '目标邮箱' })
  @IsNotEmpty({ message: '目标邮箱必填' })
  readonly email: string;

  @ApiPropertyOptional({ description: '主题' })
  readonly subject: string;

  @ApiPropertyOptional({ description: '签名' })
  readonly sign: string;
}
