import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

// 用户角色表
@Entity('sys_user_role')
export class UserRoleEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    length: 100,
    comment: '用户id',
    type: 'varchar',
  })
  user_id: string;

  @Column({
    length: 100,
    comment: '角色id',
    type: 'varchar',
  })
  role_id: string;
}
