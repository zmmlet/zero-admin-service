import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { IdDto, UserRoleDto } from './dto/user-role.dto';
import { UserRoleService } from './user-role.service';

@ApiTags('用户角色管理')
@Controller('user')
export class UserRoleController {
  constructor(private readonly userRoleService: UserRoleService) {}

  /**
   * 关联用户和角色
   * @param post
   */
  @ApiOperation({ summary: '关联用户和角色' })
  @Post('saveUserRole')
  async create(@Body() post: UserRoleDto) {
    return await this.userRoleService.create(post);
  }

  /**
   * 根据角色id查询关联用户
   * @param id
   */
  @ApiOperation({ summary: '根据角色id查询关联用户' })
  @Get('queryUserRoleList')
  async findById(@Query() query: IdDto) {
    return await this.userRoleService.queryUserRoleById(query);
  }
}
