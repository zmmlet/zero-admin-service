import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class UserRoleDto {
  @ApiProperty({ description: '角色ID' })
  @IsNotEmpty({ message: '角色ID必填' })
  role_id: string;

  @ApiProperty({ description: '用户ID' })
  @IsNotEmpty({ message: '用户ID必填' })
  user_id: string;
}

export class IdDto {
  @ApiProperty({ description: '角色id' })
  @IsNotEmpty({ message: '角色id必填' })
  readonly id: string;
}
